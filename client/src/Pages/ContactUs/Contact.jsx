import React from 'react';
import './Contact.css';
import RequestItem from '../../components/RequestItem';
import { Helmet } from 'react-helmet';

import home from '../../assets/contactus/home.png';
import contactus from '../../assets/contactus/contactus.png';
import chat from '../../assets/contactus/chat.png';

const Contact = () => {
  const handleWhatsAppClick = () => {
    // Replace '123456789' with the actual phone number
    const phoneNumber = '17986539';
    
    // Create the WhatsApp chat link
    const whatsappLink = `https://wa.me/${phoneNumber}`;
    
    // Open the link in a new tab
    window.open(whatsappLink, '_blank');
  };
  return (
      <>
        <Helmet>
        <meta charSet="utf-8" />
        <title>tshongkha-bhutan-contact</title>
        <link rel="canonical" href="http://tshongkha.com/"/>
        <meta name="description" content="Discover the allure of our best-selling Harmony Collection — a perfect blend of modern aesthetics and practical functionality. Elevate your home with this versatile and timeless piece that complements any decor effortlessly. Keywords: tshongkha, tshongkha-bhutan, harmony collection, modern aesthetics, practical functionality, versatile, timeless, home decor, hatil showroom, kitchen goods, chair, table, bed" />
        <script type="application/ld+json">
          {`
            {
              "@context": "http://schema.org",
              "@type": "LocalBusiness",
              "name": "tshongkha-bhutan",
              "description": "Discover the allure of our best-selling Harmony Collection — a perfect blend of modern aesthetics and practical functionality. Elevate your home with this versatile and timeless piece that complements any decor effortlessly.",
              "address": {
                "@type": "PostalAddress",
                "streetAddress": "Chorten Lam, 11001",
                "addressLocality": "Thimphu",
                "addressCountry": "Bhutan"
              },
              "telephone": "77792293",
              "image": "https://hatil-image.s3.ap-southeast-1.amazonaws.com/Nop_Image/HATIL%20Bed%20Epiphany-115.jpg",
              "url": "https://www.tshongkha.com/",
              "openingHours": "24/7",
              "geo": {
                "@type": "GeoCoordinates",
                "latitude": "27.469999625780257",
                "longitude": "89.63866025973961"
              },
              "sameAs": [
                "https://www.facebook.com/HatilBhutan",
                "https://www.instagram.com/tshongkha_bhutan/"
              ]
            }
          `}
        </script>
      </Helmet>
        <div className="container">
          <div className="row my-5">
            <div className="col-md-4 mt-md-0 mt-3 border-end d-flex flex-column text-center align-items-center">
                <img height={60} src={home} alt="homeicon" />
                <h6 className='contact-item-heading'>Visit Us</h6>
                <div className='mt-3'>
                  <p className='m-0 contact-us-item-text'>Monday to Sunday, 9am to 5pm </p>
                  <p className='m-0 pt-2 contact-us-item-text'>Babena, Thimphu 11001</p>
                  <p className='m-0 pt-2 contact-us-item-text'>Belpi Lam - 5</p>
                  <p className='m-0 pt-2 contact-us-item-text'>Big Mart Bulding, 4th Floor</p>
                  <p className='m-0 pt-2 contact-us-item-text'>Opposite to India house, Thimphu, Bhutan</p>
                </div>
                <p className='mt-3'>OR</p>
                <a target='_blank' href='https://maps.app.goo.gl/M9ntnxbVV87DSBXh8' className='conatact-us-item-button px-3 py-2 text-decoration-none text-dark'>
                  <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
                    <g clip-path="url(#clip0_479_155)">
                      <path d="M22.0614 11.6964V22.324L18.7215 21.7058L17.249 19.9395L11.0449 13.0423L16.3123 9.29968C18.9508 12.7824 19.034 11.9334 19.034 11.9334C19.034 11.9334 20.3374 9.98757 22.0094 11.6596L22.0614 11.6964Z" fill="#CCCCCC"/>
                      <path d="M9.31108 10.8938L20.2638 24.9903C20.1995 24.9967 20.135 24.9999 20.0704 25H2.16406L9.31108 10.8938Z" fill="#518EF8"/>
                      <path d="M16.3116 9.29973L0.0214844 23.0102V4.95066C0.0214844 3.85144 0.912793 2.96082 2.01133 2.96082H13.8907C13.3806 3.83757 15.272 4.40032 15.272 5.48777C15.272 6.5822 15.7953 8.41882 16.3116 9.29973Z" fill="#28B446"/>
                      <path d="M22.0613 22.324V23.0102C22.0613 24.0436 21.2733 24.8933 20.2649 24.9903L11.0996 15.9457L13.3182 13.6973L18.4386 18.7499L18.7214 19.0292L22.0613 22.324Z" fill="#F2F2F2"/>
                      <path d="M15.7474 11.2348L13.3175 13.6973L11.0989 15.9456L2.1645 25H2.01133C0.912793 25 0.0214844 24.1087 0.0214844 23.0102L13.9011 8.94482C14.161 9.38979 15.9484 8.93926 16.3116 9.29966L16.9791 10.1577C17.3048 10.505 15.4674 10.8661 15.7474 11.2348Z" fill="#FFD837"/>
                      <path d="M5.21968 11.1219C3.47124 11.1219 2.04883 9.69944 2.04883 7.951C2.04883 6.20256 3.47124 4.78015 5.21968 4.78015C6.06606 4.78015 6.86211 5.11013 7.46138 5.70935L6.72617 6.44441C6.32344 6.04158 5.78838 5.81975 5.21968 5.81975C4.04448 5.81975 3.08848 6.77581 3.08848 7.95095C3.08848 9.1261 4.04453 10.0822 5.21968 10.0822C6.21558 10.0822 7.0542 9.39553 7.28677 8.47073H5.21968V7.43113H8.39053V7.95095C8.39048 9.69944 6.96811 11.1219 5.21968 11.1219Z" fill="white"/>
                      <path d="M13.8917 2.96084C14.9209 1.19072 16.8387 0 19.0344 0C22.3175 0 24.9789 2.66143 24.9789 5.94453C24.9789 6.76792 24.8112 7.55176 24.509 8.26494C24.208 8.97269 23.7729 9.61545 23.2275 10.1577C22.777 10.6366 22.3729 11.1426 22.0098 11.6596C19.6304 15.0495 19.0344 18.9196 19.0344 18.9196C19.0344 18.9196 18.3871 14.7175 15.7485 11.2348C15.4685 10.8661 15.1663 10.5049 14.8406 10.1577H14.8413C14.4768 9.79598 14.1611 9.38825 13.9021 8.94483C13.3858 8.06392 13.0898 7.03887 13.0898 5.94448C13.0898 4.85708 13.3816 3.83755 13.8917 2.96084Z" fill="#F14336"/>
                      <path d="M19.0331 2.78064C20.7802 2.78064 22.1962 4.19749 22.1962 5.9446C22.1962 7.69172 20.7802 9.10774 19.0331 9.10774C17.286 9.10774 15.8691 7.69172 15.8691 5.9446C15.8691 4.19749 17.286 2.78064 19.0331 2.78064Z" fill="#7E2D25"/>
                    </g>
                    <defs>
                      <clipPath id="clip0_479_155">
                        <rect width="25" height="25" fill="white"/>
                      </clipPath>
                    </defs>
                  </svg>
                  <span className='ps-3'>Google Map Location</span>
                  </a>
                
            </div>
            <div className="col-md-4 mt-md-0 mt-3  border-end d-flex flex-column align-items-center text-center">
              <img height={60} src={contactus} alt="contacticon" />
              <h6 className='contact-item-heading'>Contact Us</h6>
              <div className='mt-3'>
                  <p className='m-0 contact-us-item-text'>+975 17986539</p>
                  <p className='m-0 pt-2 contact-us-item-text'>sonamtoeb@gmail.com</p>
              </div>
                <p className='mt-3'>OR</p>
              <a target='_blank' href='https://www.instagram.com/tshongkha_bhutan/' className='conatact-us-item-button text-decoration-none text-dark px-3 py-2'>
              <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
                <path d="M19.1406 0H5.85938C2.62333 0 0 2.62333 0 5.85938V19.1406C0 22.3767 2.62333 25 5.85938 25H19.1406C22.3767 25 25 22.3767 25 19.1406V5.85938C25 2.62333 22.3767 0 19.1406 0Z" fill="url(#paint0_radial_479_183)"/>
                <path d="M19.1406 0H5.85938C2.62333 0 0 2.62333 0 5.85938V19.1406C0 22.3767 2.62333 25 5.85938 25H19.1406C22.3767 25 25 22.3767 25 19.1406V5.85938C25 2.62333 22.3767 0 19.1406 0Z" fill="url(#paint1_radial_479_183)"/>
                <path d="M12.5009 2.73438C9.84873 2.73438 9.51582 2.746 8.47422 2.79336C7.43457 2.84102 6.7249 3.00557 6.104 3.24707C5.46162 3.49648 4.9168 3.83018 4.37402 4.37314C3.83076 4.91602 3.49707 5.46084 3.24688 6.10293C3.00469 6.72402 2.83994 7.43398 2.79316 8.47314C2.74658 9.51484 2.73438 9.84785 2.73438 12.5001C2.73438 15.1523 2.74609 15.4842 2.79336 16.5258C2.84121 17.5654 3.00576 18.2751 3.24707 18.896C3.49668 19.5384 3.83037 20.0832 4.37334 20.626C4.91602 21.1692 5.46084 21.5037 6.10273 21.7531C6.72412 21.9946 7.43389 22.1592 8.47334 22.2068C9.51504 22.2542 9.84766 22.2658 12.4997 22.2658C15.1521 22.2658 15.484 22.2542 16.5256 22.2068C17.5652 22.1592 18.2757 21.9946 18.8971 21.7531C19.5392 21.5037 20.0832 21.1692 20.6258 20.626C21.169 20.0832 21.5026 19.5384 21.7529 18.8963C21.993 18.2751 22.1578 17.5652 22.2066 16.526C22.2534 15.4844 22.2656 15.1523 22.2656 12.5001C22.2656 9.84785 22.2534 9.51504 22.2066 8.47334C22.1578 7.43369 21.993 6.72412 21.7529 6.10322C21.5026 5.46084 21.169 4.91602 20.6258 4.37314C20.0826 3.82998 19.5394 3.49629 18.8965 3.24717C18.2739 3.00557 17.5639 2.84092 16.5242 2.79336C15.4825 2.746 15.1509 2.73438 12.4979 2.73438H12.5009ZM11.6248 4.49424C11.8849 4.49385 12.175 4.49424 12.5009 4.49424C15.1084 4.49424 15.4174 4.50361 16.4471 4.55039C17.3992 4.59395 17.916 4.75303 18.2603 4.88672C18.716 5.06367 19.0409 5.27529 19.3825 5.61719C19.7243 5.95898 19.9358 6.28447 20.1133 6.74023C20.247 7.08398 20.4062 7.60078 20.4496 8.55293C20.4964 9.58242 20.5065 9.8916 20.5065 12.4979C20.5065 15.1041 20.4964 15.4134 20.4496 16.4428C20.4061 17.3949 20.247 17.9117 20.1133 18.2556C19.9363 18.7113 19.7243 19.0358 19.3825 19.3774C19.0407 19.7192 18.7162 19.9308 18.2603 20.1078C17.9164 20.2421 17.3992 20.4008 16.4471 20.4443C15.4176 20.4911 15.1084 20.5013 12.5009 20.5013C9.89326 20.5013 9.58418 20.4911 8.55478 20.4443C7.60264 20.4004 7.08584 20.2413 6.74131 20.1076C6.28564 19.9306 5.96006 19.719 5.61826 19.3772C5.27646 19.0354 5.06494 18.7107 4.8875 18.2548C4.75381 17.9109 4.59453 17.3941 4.55117 16.442C4.50439 15.4125 4.49502 15.1033 4.49502 12.4954C4.49502 9.8876 4.50439 9.57998 4.55117 8.55049C4.59473 7.59834 4.75381 7.08154 4.8875 6.7373C5.06455 6.28154 5.27646 5.95605 5.61836 5.61426C5.96016 5.27246 6.28564 5.06084 6.74141 4.8835C7.08564 4.74922 7.60264 4.59053 8.55478 4.54678C9.45566 4.50605 9.80479 4.49385 11.6248 4.4918V4.49424ZM17.7138 6.11572C17.0668 6.11572 16.5419 6.64014 16.5419 7.28721C16.5419 7.93418 17.0668 8.45908 17.7138 8.45908C18.3607 8.45908 18.8856 7.93418 18.8856 7.28721C18.8856 6.64023 18.3607 6.11533 17.7138 6.11533V6.11572ZM12.5009 7.48496C9.73135 7.48496 7.48584 9.73047 7.48584 12.5001C7.48584 15.2697 9.73135 17.5142 12.5009 17.5142C15.2705 17.5142 17.5152 15.2697 17.5152 12.5001C17.5152 9.73057 15.2703 7.48496 12.5007 7.48496H12.5009ZM12.5009 9.24482C14.2986 9.24482 15.7562 10.7021 15.7562 12.5001C15.7562 14.2979 14.2986 15.7554 12.5009 15.7554C10.703 15.7554 9.2457 14.2979 9.2457 12.5001C9.2457 10.7021 10.703 9.24482 12.5009 9.24482Z" fill="white"/>
                <defs>
                  <radialGradient id="paint0_radial_479_183" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(6.64062 26.9255) rotate(-90) scale(24.7769 23.0444)">
                    <stop stop-color="#FFDD55"/>
                    <stop offset="0.1" stop-color="#FFDD55"/>
                    <stop offset="0.5" stop-color="#FF543E"/>
                    <stop offset="1" stop-color="#C837AB"/>
                  </radialGradient>
                  <radialGradient id="paint1_radial_479_183" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(-4.1876 1.80088) rotate(78.681) scale(11.0754 45.6531)">
                    <stop stop-color="#3771C8"/>
                    <stop offset="0.128" stop-color="#3771C8"/>
                    <stop offset="1" stop-color="#6600FF" stop-opacity="0"/>
                  </radialGradient>
                </defs>
              </svg>
                  <span className='ps-3'>instagram.com/tshongkha_bhutan/</span>
              </a>
              <a target='_blank' href='https://www.facebook.com/HatilBhutan/' className='conatact-us-item-button text-decoration-none text-dark px-3 py-2 mt-3'>
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
                  <path d="M23.6206 0H1.37938C0.617571 0 0 0.617571 0 1.37938V23.6206C0 24.3824 0.617571 25 1.37938 25H23.6206C24.3824 25 25 24.3824 25 23.6206V1.37938C25 0.617571 24.3824 0 23.6206 0Z" fill="#3D5A98"/>
                  <path d="M17.2482 24.9979V15.3168H20.4971L20.9829 11.5441H17.2482V9.13601C17.2482 8.04391 17.5524 7.29824 19.1177 7.29824H21.116V3.91843C20.1483 3.81758 19.1759 3.76963 18.203 3.77479C15.326 3.77479 13.3445 5.52806 13.3445 8.76212V11.5441H10.0957V15.3168H13.3445V24.9979H17.2482Z" fill="white"/>
                </svg>
                  <span className='ps-3'>facebook.com/HatilBhutan</span>
              </a>
            </div>
            <div className="col-md-4 mt-md-0 mt-3  d-flex flex-column align-items-center text-center">
              <img height={60} src={chat} alt="chatIcon" />
              <h6 className='contact-item-heading'>Live Chat</h6>
              <div className='mt-3'>
                  <p className='m-0 contact-us-item-text px-3'>Chat with a member of our in-house team for queries.</p>
                </div>
                <button onClick={handleWhatsAppClick} className='conatact-us-item-button px-3 py-2 mt-5'>
                  <svg xmlns="http://www.w3.org/2000/svg" width="21" height="20" viewBox="0 0 21 20" fill="none">
                    <path d="M0.447773 9.91326C0.447281 11.5992 0.911906 13.2454 1.79538 14.6964L0.363281 19.6541L5.71434 18.3238C7.19438 19.0877 8.85265 19.488 10.5378 19.4881H10.5422C16.1052 19.4881 20.6335 15.1962 20.6359 9.9208C20.637 7.36452 19.588 4.96076 17.682 3.1523C15.7765 1.344 13.2421 0.347602 10.5418 0.346436C4.9782 0.346436 0.450152 4.63814 0.447855 9.91326" fill="url(#paint0_linear_479_176)"/>
                    <path d="M0.0877734 9.90997C0.0871992 11.6566 0.568476 13.3617 1.48345 14.8646L0 20L5.54293 18.622C7.07019 19.4115 8.78973 19.8278 10.5395 19.8284H10.544C16.3065 19.8284 20.9975 15.3821 21 9.91791C21.001 7.26977 19.9142 4.7796 17.9402 2.90635C15.966 1.03334 13.341 0.00108887 10.544 0C4.78045 0 0.0900703 4.44571 0.0877734 9.90997ZM3.38879 14.6058L3.18183 14.2943C2.3118 12.9827 1.85259 11.467 1.85325 9.9106C1.85505 5.36884 5.75351 1.67375 10.5473 1.67375C12.8687 1.67468 15.0504 2.53271 16.6914 4.08949C18.3323 5.64642 19.2352 7.71605 19.2346 9.91728C19.2325 14.459 15.3339 18.1546 10.544 18.1546H10.5405C8.98086 18.1538 7.45123 17.7567 6.11723 17.0062L5.79977 16.8277L2.51048 17.6454L3.38879 14.6058Z" fill="url(#paint1_linear_479_176)"/>
                    <path d="M7.93012 5.76702C7.73439 5.35457 7.52841 5.34625 7.34228 5.33902C7.18987 5.33279 7.01563 5.33326 6.84156 5.33326C6.66733 5.33326 6.38424 5.3954 6.14495 5.64312C5.90542 5.89107 5.23047 6.49026 5.23047 7.70894C5.23047 8.9277 6.16669 10.1055 6.2972 10.2709C6.42788 10.436 8.1046 13.017 10.7601 14.0099C12.9671 14.835 13.4162 14.6709 13.8952 14.6295C14.3742 14.5883 15.441 14.0305 15.6586 13.452C15.8764 12.8737 15.8764 12.3779 15.8111 12.2743C15.7458 12.1711 15.5716 12.1092 15.3103 11.9853C15.049 11.8614 13.7645 11.2622 13.5251 11.1795C13.2855 11.0969 13.1114 11.0557 12.9371 11.3037C12.7629 11.5513 12.2626 12.1092 12.1101 12.2743C11.9578 12.4399 11.8053 12.4605 11.5441 12.3366C11.2827 12.2124 10.4413 11.9511 9.44302 11.1073C8.66635 10.4507 8.142 9.6399 7.98959 9.39187C7.83718 9.14423 7.97327 9.00999 8.10427 8.88656C8.22166 8.77557 8.36562 8.59731 8.49638 8.45272C8.62664 8.30806 8.67012 8.20485 8.75724 8.03965C8.84444 7.8743 8.8008 7.72963 8.73558 7.60573C8.67012 7.48184 8.16243 6.25678 7.93012 5.76702Z" fill="white"/>
                    <defs>
                      <linearGradient id="paint0_linear_479_176" x1="1013.99" y1="1931.11" x2="1013.99" y2="0.346436" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#1FAF38"/>
                        <stop offset="1" stop-color="#60D669"/>
                      </linearGradient>
                      <linearGradient id="paint1_linear_479_176" x1="1050" y1="2000" x2="1050" y2="0" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#F9F9F9"/>
                        <stop offset="1" stop-color="white"/>
                      </linearGradient>
                    </defs>
                  </svg>
                  <span className='ps-3'>Start Chat</span>
                </button>
            </div>
          </div>
        </div>
        <RequestItem/>
      </>
  )
}

export default Contact